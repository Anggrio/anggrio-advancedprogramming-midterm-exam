package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.model.User;
import id.ac.ui.cs.advprog.midterm.model.UserDTO;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class UserController {

    private static final String ADD_USER = "add-user";
    private static final String INDEX = "index";
    private static final String INVALID_USER_ID = "Invalid user Id: ";
    private static final String MESSAGE = "message";
    private static final String UPDATE_USER = "update-user";
    private static final String USERS = "users";
    private static final String REDIRECT_CUSTOM_ERROR_PAGE = "redirect:/custom-error";

    @Autowired
    UserRepository userRepository;

    @GetMapping("/")
    public String home(Model model) {
        if(userRepository.count() != 0) {
            model.addAttribute(USERS, userRepository.findAll());
        }

        return INDEX;
    }

    @GetMapping("/signup")
    public String showSignUpForm(@ModelAttribute("user") UserDTO user) {
        return ADD_USER;
    }

    @GetMapping("/adduser")
    public String addUserRedirect() {
        return "redirect:/";
    }

    @PostMapping("/adduser")
    public String addUser(@ModelAttribute("user") @Valid UserDTO user, BindingResult result, Model model) {
        User persistentUser = new User();
        BeanUtils.copyProperties(user, persistentUser);

        if (result.hasErrors()) {
            return ADD_USER;
        }

        userRepository.save(persistentUser);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") long id, Model model, RedirectAttributes redirectAttributes) {
        if (!userRepository.existsById(id)) {
            redirectAttributes.addFlashAttribute(MESSAGE, INVALID_USER_ID + id);
            return REDIRECT_CUSTOM_ERROR_PAGE;
        }

        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(INVALID_USER_ID + id));

        model.addAttribute("user", user);
        return UPDATE_USER;
    }

    @GetMapping("/update/{id}")
    public String updateUserRedirect() {
        return "redirect:/";
    }

    @PostMapping("/update/{id}")
    public String updateUser(@PathVariable("id") long id, @ModelAttribute("user") @Valid UserDTO user, BindingResult result, Model model, RedirectAttributes redirectAttributes) {
        if (!userRepository.existsById(id)) {
            redirectAttributes.addFlashAttribute(MESSAGE, INVALID_USER_ID + id);
            return REDIRECT_CUSTOM_ERROR_PAGE;
        }

        User persistentUser = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(INVALID_USER_ID + id));
        BeanUtils.copyProperties(user, persistentUser);

        if (result.hasErrors()){
            persistentUser.setId(id);
            model.addAttribute("user", persistentUser);
            return UPDATE_USER;
        }

        userRepository.save(persistentUser);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") long id, Model model, RedirectAttributes redirectAttributes) {
        if (!userRepository.existsById(id)) {
            redirectAttributes.addFlashAttribute(MESSAGE, INVALID_USER_ID + id);
            return REDIRECT_CUSTOM_ERROR_PAGE;
        }

        User user = userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException(INVALID_USER_ID + id));

        userRepository.delete(user);
        model.addAttribute(USERS, userRepository.findAll());
        return INDEX;
    }

    @GetMapping("/custom-error")
    public String customErrorPage(Model model) {
        return "error";
    }
}
