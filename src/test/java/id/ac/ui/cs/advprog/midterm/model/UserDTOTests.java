package id.ac.ui.cs.advprog.midterm.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserDTOTests {

    private UserDTO userDTO;

    @BeforeEach
    public void setUp() {
        userDTO = new UserDTO("Rio", "anggriosutopo@gmail.com");
    }

    @Test
    public void whenGetEmailIsCalledReturnUserEmail() {
        assertEquals("anggriosutopo@gmail.com", userDTO.getEmail());
    }

    @Test
    public void whenGetNameIsCalledReturnUserName() {
        assertEquals("Rio", userDTO.getName());
    }

    @Test
    public void whenSetEmailIsCalledChangeUserEmail() {
        userDTO.setEmail("anggrio@gmail.com");
        assertEquals("anggrio@gmail.com", userDTO.getEmail());
    }

    @Test
    public void whenSetNameIsCalledChangeUserName() {
        userDTO.setName("Anggrio");
        assertEquals("Anggrio", userDTO.getName());
    }
}
