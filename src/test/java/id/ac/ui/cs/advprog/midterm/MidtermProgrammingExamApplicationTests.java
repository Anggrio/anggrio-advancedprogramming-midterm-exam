package id.ac.ui.cs.advprog.midterm;

import id.ac.ui.cs.advprog.midterm.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class MidtermProgrammingExamApplicationTests {

	@Autowired
	private UserController UserController;

	@Test
	public void contextLoads() throws Exception {
		assertThat(UserController).isNotNull();
	}

	@Test
	public void testMainMethodRuns() {
		MidtermProgrammingExamApplication.main(new String[] {});
		assertTrue(true, "Test for main method");
	}
}
