package id.ac.ui.cs.advprog.midterm.controller;

import id.ac.ui.cs.advprog.midterm.model.UserDTO;
import id.ac.ui.cs.advprog.midterm.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTests {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenEmptyURLIsAccessedAndRepositoryEmptyShouldReturnIndex(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenEmptyURLIsAccessedAndRepositoryNotEmptyShouldReturnIndex(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO("Rio", "anggriosutopo@gmail.com")))
                .andDo(
                        result -> mockMvc.perform(get("/"))
                                .andExpect(status().isOk())
                                .andExpect(view().name("index"))
                );
    }

    @Test
    public void whenSignUpURLIsAccessedShouldReturnAddUser(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/signup")
                .flashAttr("user", new UserDTO()))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void whenAddUserURLIsAccessedByGetShouldRedirectToEmptyURL(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/adduser"))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void whenAddUserRULIsAccessedWithIncorrectAttributeShouldReturnAddUser(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO()))
                .andExpect(status().isOk())
                .andExpect(view().name("add-user"));
    }

    @Test
    public void whenAddUserURLIsAccessedWithCorrectInputShouldReturnIndex(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO("Rio", "anggriosutopo@gmail.com")))
                .andExpect(status().isOk())
                .andExpect(view().name("index"));
    }

    @Test
    public void whenEditIdURLIsAccessedWithIncorrectInputShouldRedirectToCustomErrorURL(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/edit/{id}",1))
                .andExpect(redirectedUrl("/custom-error"));
    }

    @DirtiesContext
    @Test
    public void whenEditIdURLIsAccessedWithCorrectInputShouldReturnUpdateUser(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO("Rio", "anggriosutopo@gmail.com")))
                .andDo(
                        result -> mockMvc.perform(get("/edit/{id}",1))
                                .andExpect(status().isOk())
                                .andExpect(view().name("update-user"))
                );
    }

    @Test
    public void whenUpdateIdURLIsAccessedWithGetShouldRedirectToEmptyURL(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/update/{id}",1))
                .andExpect(redirectedUrl("/"));
    }

    @Test
    public void whenUpdateIdURLIsAccessedWithIncorrectInputShouldRedirectToCustomErrorURL(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/update/{id}",1)
                .flashAttr("user", new UserDTO()))
                .andExpect(redirectedUrl("/custom-error"));
    }

    @Test
    public void whenUpdateIdURLIsAccessedWithIncorrectInputAndAttributeShouldReturnUpdateUser(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO("Rio", "anggriosutopo@gmail.com")))
                .andDo(
                        result -> mockMvc.perform(post("/update/{id}",1)
                                .flashAttr("user", new UserDTO()))
                                .andExpect(status().isOk())
                                .andExpect(view().name("update-user"))
                );
    }

    @DirtiesContext
    @Test
    public void whenUpdateIdURLIsAccessedWithCorrectInputShouldReturnIndex(@Autowired MockMvc mockMvc) throws Exception {
        UserDTO userDTO = new UserDTO("Rio", "anggriosutopo@gmail.com");
        mockMvc.perform(post("/adduser")
                .flashAttr("user", userDTO))
                .andDo(
                        result -> mockMvc.perform(post("/update/{id}",1)
                                .flashAttr("user", userDTO))
                                .andExpect(status().isOk())
                                .andExpect(view().name("index"))
                );
    }

    @Test
    public void whenDeleteIdURLIsAccessedWithIncorrectInputShouldRedirectToCustomErrorURL(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/delete/{id}",1))
                .andExpect(redirectedUrl("/custom-error"));
    }

    @Test
    public void whenDeleteIdURLIsAccessedWithCorrectInputShouldReturnIndex(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(post("/adduser")
                .flashAttr("user", new UserDTO("Rio", "anggriosutopo@gmail.com")))
                .andDo(
                        result -> mockMvc.perform(get("/delete/{id}",1))
                                .andExpect(status().isOk())
                                .andExpect(view().name("index"))
                );
    }

    @DirtiesContext
    @Test
    public void whenCustomErrorURLIsAccessedShouldReturnError(@Autowired MockMvc mockMvc) throws Exception {
        mockMvc.perform(get("/custom-error"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"));
    }
}
