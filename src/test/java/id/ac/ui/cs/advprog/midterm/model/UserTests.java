package id.ac.ui.cs.advprog.midterm.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class UserTests {

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User("Rio", "anggriosutopo@gmail.com");
    }

    @Test
    public void whenGetIdIsCalledReturnUserId() {
        assertEquals(0, (long) user.getId());
    }

    @Test
    public void whenGetEmailIsCalledReturnUserEmail() {
        assertEquals("anggriosutopo@gmail.com", user.getEmail());
    }

    @Test
    public void whenGetNameIsCalledReturnUserName() {
        assertEquals("Rio", user.getName());
    }

    @Test
    public void whenSetIdIsCalledChangeUserId() {
        user.setId((long) 1);
        assertEquals(1, (long) user.getId());
    }

    @Test
    public void whenSetEmailIsCalledChangeUserEmail() {
        user.setEmail("anggrio@gmail.com");
        assertEquals("anggrio@gmail.com", user.getEmail());
    }

    @Test
    public void whenSetNameIsCalledChangeUserName() {
        user.setName("Anggrio");
        assertEquals("Anggrio", user.getName());
    }

    @Test
    public void whenToStringIsCalledReturnStringForm() {
        String expectedString = "User Id: 0\nName: Rio\nEmail: anggriosutopo@gmail.com";
        assertEquals(expectedString, user.toString());
    }
}
